#
# Cookbook Name:: lwrp
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

=begin
	
rescue Exception => e
	
end
lwrp 'exampleconfig' do   #This is how we utilize the default.rb LWRP files we created.  It's simply the cookbook name: '-' converts to '_'.
  string_param 'this is a string'
  boolean_param true
  array_param ['a', 'b', 'c']
  hash_param ({
    'a' => '2',
    'b' => '3'
  })
  action :create
end
=end

lwrp 'lwrp example' do
  action :delete
  string_param 'this is a string'
end