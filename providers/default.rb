action :create do
  converge_by("Creating.") do
      resp = create
      @new_resource.updated_by_last_action(resp)  #We set our updated flag based on the resource we utilized.
    end
end

action :delete do
  converge_by("Deleting.") do
      resp = delete
      @new_resource.updated_by_last_action(resp)
    end
end

#Our Methods
def create
  puts "hi i am with create function"
end

def delete
  puts "hi i am with delete function with string parameter : "+new_resource.string_param
end